<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required|string',
            'lname' => 'required|string',
            'course' => 'required|string',
            'institute' => 'required|string',
            'addinfo' => 'required|string',
            'zcode' => 'required|string',
            'place' => 'required|string',
            'country' => 'required|string',
            'ccode' => 'required|string',
            'pnumber' => 'required|string',
            'email' => 'required|string',


        ];
    }
}
