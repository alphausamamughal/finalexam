@extends('layouts.layout')

@section('title')
    Edit Category
@endsection

@section('css')
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('content')
<main id="main" class="main">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
        {{ $error }}
    @endforeach
@endif
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Category
                </h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Categories</a></li>
                    <li class="breadcrumb-item active">
                        Category
                    </li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <!-- jquery validation -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">
                            Category
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form id="quickForm" method="POST"
                        action="{{ route('categories.update', ['category' => $category->id]) }}"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="InputSnumber">S. No.</label>
                                <input type="text" name="snumber" value="{{ $category->snumber }}"
                                    class="form-control @if ($errors->has('snumber')) is-invalid @endif"
                                    id="InputSnumber" placeholder="Enter S. No." aria-describedby="SnumberError"
                                    aria-invalid="true">
                                <span id="SnumberError" class="error invalid-feedback">
                                    @if ($errors->has('snumber'))
                                        {{ $errors->first('snumber') }}
                                    @endif
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="Inputnobooks">No. of Books</label>
                                <input type="text" name="nobooks" value="{{ $category->nobooks }}"
                                    class="form-control @if ($errors->has('nobooks')) is-invalid @endif"
                                    id="Inputnobooks" placeholder="Enter No. of Books" aria-describedby="nobooksError"
                                    aria-invalid="true">
                                <span id="nobooksError" class="error invalid-feedback">
                                    @if ($errors->has('nobooks'))
                                        {{ $errors->first('nobooks') }}
                                    @endif
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="Inputaname">Arthur Name</label>
                                <input type="text" name="aname" value="{{ $category->aname }}"
                                    class="form-control @if ($errors->has('aname')) is-invalid @endif"
                                    id="Inputaname" placeholder="Enter S. No." aria-describedby="anameError"
                                    aria-invalid="true">
                                <span id="anameError" class="error invalid-feedback">
                                    @if ($errors->has('aname'))
                                        {{ $errors->first('aname') }}
                                    @endif
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="InputCode">Code</label>
                                <input type="text" name="code" value="{{ $category->code }}"
                                    class="form-control @if ($errors->has('code')) is-invalid @endif"
                                    id="Inputcode" placeholder="Enter Code" aria-describedby="CodeError"
                                    aria-invalid="true">
                                <span id="codeError" class="error invalid-feedback">
                                    @if ($errors->has('code'))
                                        {{ $errors->first('code') }}
                                    @endif
                                </span>
                            </div>

                            <div class="form-group">
                                <label for="InputCLass">Class</label>
                                <input type="text" name="class" value="{{ $category->class }}"
                                    class="form-control @if ($errors->has('class')) is-invalid @endif"
                                    id="InputClass" placeholder="Enter Class" aria-describedby="classError"
                                    aria-invalid="true">
                                <span id="ClassError" class="error invalid-feedback">
                                    @if ($errors->has('class'))
                                        {{ $errors->first('class') }}
                                    @endif
                                </span>
                            </div>





                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">

            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>




      </main><!-- End #main -->

@endsection

@section('scripts')
    <script src="{{ asset('plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
@endsection
