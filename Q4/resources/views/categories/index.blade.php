@extends('layouts.layout')

@section('title')
    Categories
@endsection

@section('css')
    @include('includes.datatable_css')
@endsection

@section('content')
    <main id="main" class="main">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Categories of Books</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Categories</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Categories</h3>
                                <a href="{{ route('categories.create') }}" class="btn btn-primary float-right"> Add
                                    category</a>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>No. of Books</th>
                                            <th>Arthur Name</th>
                                            <th>Code</th>
                                            <th>CLass</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($categories as $category)
                                            <tr>
                                                <td>{{ $category->snumber }}</td>
                                                <td>{{ $category->nobooks }}</td>
                                                <td>{{ $category->aname }}</td>
                                                <td>{{ $category->code }}</td>
                                                <td>{{ $category->class }}</td>




                                                <td>
                                                    <a class="btn btn-primary"
                                                        href="{{ route('categories.edit', ['category' => $category->id]) }}">Update</a>
                                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                                        data-target="#deleteModal{{ $category->id }}">
                                                        Delete
                                                    </button>
                                                    <div class="modal fade" id="deleteModal{{ $category->id }}"
                                                        style="display: none ;" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Warning</h4>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">×</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>This action is irreversible. Are You Sure , You want
                                                                        to
                                                                        delete this category permanently ?</p>
                                                                </div>
                                                                <form
                                                                    action="{{ route('categories.destroy', ['category' => $category->id]) }}"
                                                                    method="POST">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <div class="modal-footer justify-content-between">
                                                                        <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close</button>
                                                                        <button type="submit"
                                                                            class="btn btn-primary">Delete</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>No. of Books</th>
                                            <th>Arthur Name</th>
                                            <th>Code</th>
                                            <th>CLass</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- /.content -->


    </main><!-- End #main -->
@endsection
@section('scripts')
    @include('includes.datatable_scripts')
@endsection
