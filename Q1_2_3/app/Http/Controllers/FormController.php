<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRequest;
use App\Models\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        return view('form');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function form(CreateRequest $req)
    {

    $users = new Form();
    $users->fname = $req->get('fname');
    $users->lname = $req->get('lname');
    $users->course = $req->get('course');
    $users->institute = $req->get('institute');
    $users->addinfo = $req->get('addinfo');
    $users->zcode = $req->get('zcode');
    $users->place = $req->get('place');
    $users->country = $req->get('country');
    $users->ccode = $req->get('ccode');
    $users->pnumber = $req->get('pnumber');
    $users->email = $req->get('email');


        $users->save();
        return redirect('/') ->with('message', 'Form Created Successfully')->with('message_type', 'info');;

    }


}
