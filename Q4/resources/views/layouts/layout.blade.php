<!DOCTYPE html>
<html lang="en">

{{-- <---------head-tag---------> --}}
@include('includes.head')

<body>

{{-- <---------headear---------> --}}
@include('includes.header')
@include('includes.asidebar')
{{-- <---------content---------> --}}
@yield('content')

{{-- <---------footer---------> --}}
@include('includes.footer')

{{-- <---------scripts---------> --}}
@include('includes.footlinks')

</body>

</html>
