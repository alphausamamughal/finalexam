<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>

<!-- Vendor JS Files -->
<script src="{{ asset('assetss/vendor/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{ asset('assetss/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assetss/vendor/chart.js/chart.min.js')}}"></script>
<script src="{{ asset('assetss/vendor/echarts/echarts.min.js')}}"></script>
<script src="{{ asset('assetss/vendor/quill/quill.min.js')}}"></script>
<script src="{{ asset('assetss/vendor/simple-datatables/simple-datatables.js')}}"></script>
<script src="{{ asset('assetss/vendor/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('assetss/vendor/php-email-form/validate.js')}}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assetss/js/main.js')}}"></script>
<script>
$(function() {
      @if(session('message'))
        @if(session('message_type') && session('message_type') == 'success')
            toastr.success('{{session('message')}}')
        @elseif(session('message_type') && session('message_type') == 'info')
            toastr.info('{{session('message')}}');
        @elseif(session('message_type') && session('message_type') == 'warning')
            toastr.warning('{{session('message')}}');
        @elseif(session('message_type') && session('message_type') == 'error')
            toastr.error('{{session('message')}}');
        @else
            toastr.info('{{session('message')}}');
        @endif
      @endif
      });
</script>
